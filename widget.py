
import subprocess
from ajenti.api import plugin
from ajenti.plugins.dashboard.api import ConfigurableWidget
from ajenti.util import str_fsize
from ajenti.ui import on



@plugin
class TrafficWidget (ConfigurableWidget):
    name = _('systemctl')
    icon = 'play'
    isRunning = False
    serviceName = ''

    def refreshStatus(self):
        self.find('serviceName').text = self.serviceName
        systemCommand = '/bin/systemctl show %s | grep SubState' % self.serviceName
        
        try:
            out_bytes = subprocess.check_output( systemCommand, shell=True )
            out_text = out_bytes.decode('utf-8')
            serviceState = out_text.split( "=" )
            self.find('SubState').text = serviceState[1]
            if str(serviceState[1]) == 'running\n':
                self.isRunning = True
            else:
                self.isRunning = False
        except subprocess.CalledProcessError:
            self.find('SubState').text = 'Error systemd not exist: %s' % systemCommand
        except:
            self.find('SubState').text = 'Error'

        # show buttons
        self.find('cmdStart').visible = not self.isRunning
        self.find('cmdStop').visible = self.isRunning
        self.find('cmdRestart').visible = self.isRunning

    def serviceStart(self):
        systemCommand = '/bin/systemctl start %s ' % self.serviceName
        subprocess.call( systemCommand, shell=True )

    def serviceStop(self):
        systemCommand = '/bin/systemctl stop %s ' % self.serviceName
        subprocess.call( systemCommand, shell=True )



    def on_prepare(self):
        self.append(self.ui.inflate('systemctl:widget'))

    def on_start(self):
        # load service name
        self.serviceName = self.config['service']
        self.refreshStatus()

    def create_config(self):
        return {'service': ''}

# when config dialog popups
    def on_config_start(self):
        self.dialog.find('service').value = self.config['service']

    def on_config_save(self):
        self.config['service'] = self.dialog.find('service').value
        self.refreshStatus()

    @on('cmdStart', 'click')
    def on_cmdStart(self):
        self.serviceStart()
        self.refreshStatus()

    @on('cmdStop', 'click')
    def on_cmdStop(self):
        self.serviceStop()
        self.refreshStatus()

    @on('cmdRestart', 'click')
    def on_cmdRestart(self):
        self.serviceStop()
        self.serviceStart()
        self.refreshStatus()






