from ajenti.api import *
from ajenti.plugins import *


info = PluginInfo(
    title='systemctl',
    icon='play',
    dependencies=[
        PluginDependency('main'),
        PluginDependency('dashboard'),
    ],
)


def init():
    import widget

